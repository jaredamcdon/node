const express = require ('express')
const app = express()
const port = 1222

app.get('/', (req, res) => res.send('Hello World!'))

app.listen(port, ()=> console.log(`app listening at http://0.0.0.0:${port}`))
