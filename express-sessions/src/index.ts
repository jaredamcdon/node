import express, { Application, Request, Response } from 'express';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import { createClient } from 'redis';

const dbPass = process.env.PASSWORD as string;
const dbHost = process.env.HOST as string;
const dbPort = process.env.PORT as string;



const redisClient = createClient({
    url: `redis://default:${dbPass}@${dbHost}:${dbPort}`
})

redisClient.on('error', (err: any) => console.log('Redis client error', err))

const redisConnect = async() => {
    await redisClient.connect()
        .then(() => {
            console.log('client connected')
        })
}
redisConnect()

const app: Application = express()
    .use(express.json())
    .use(cookieParser('signingStr'))
    // this also works but I don't fully understand whats being signed/validated
    //.use(cookieParser(['signingStr', 'secondSigningStr']))
//    .use(session({

//    }))


const HOST: string = '0.0.0.0';
const PORT: number = 4000;

app.get('/', async(req: Request, res: Response) => {
    const cookie: {
        [key: string]: string
    } = {
        'timestamp': `${(new Date()).toString()}`
    }
    res.cookie(Object.entries(cookie)[0][0], Object.entries(cookie)[0][1], {} )
    redisClient.set(Object.entries(cookie)[0][0], Object.entries(cookie)[0][1])
    console.log(`${Object.entries(cookie)[0][0]}: ${Object.entries(cookie)[0][1]}`)
    return res.status(200).send('simple text response')
})

app.get('/addCookies', async (req: Request, res: Response) => {
    const cookies: {
        [key: string]: string
    } = {
        'cookie1': 'value1',
        'cookie2': 'value2'
    }
    for(let i =0; i < Object.keys(cookies).length; i++ ){
        res.cookie(Object.entries(cookies)[i][0], Object.entries(cookies)[i][1], {} )
        redisClient.set(Object.entries(cookies)[i][0], Object.entries(cookies)[i][1])
        console.log(`${Object.entries(cookies)[i][0]}: ${Object.entries(cookies)[i][1]}`)
    }
    return res.status(200).send('two cookies added')
})

app.get('/showcookies', async(req: Request, res: Response) => {
    return res.status(200).send(req.cookies)
})

app.get('/showsignedcookies', async(req: Request, res: Response) => {
    return res.status(200).send(req.signedCookies)
})

app.get('/deletecookies', async(req: Request, res: Response) => {
    console.log(req.cookies)
    console.log(typeof req.cookies)
    const cookieKeys = Object.keys(req.cookies)
    for (let i in cookieKeys){
        console.log(cookieKeys[i])
        res.clearCookie(cookieKeys[i])
        const deld = redisClient.del(cookieKeys[i])
	console.log(deld)
    }
    //res.clearCookie(req.cookies)
    res.status(200).send('cookies deleted')

})

/* ading login faking */

//md5 hash daily the previou hash
// store hashes in mysql?
/*

table
=========
cookies

ID PK INT(4),
signature CHAR(32),
date DATE

*/

interface ILogin {
    username: string;
    password: string;
}

const createToken = async(): Promise<string> => {
    let id = (Math.random() * 100000000000000000).toString()
    if (await redisClient.get(id)){
        id = await createToken()
    }
    return id
}

// signed requires valid signature on authentication
app.get('/login/:user/:pass', async(req: Request, res: Response) => {
    const payload: ILogin = {
        username: req.params.user,
        password: req.params.pass
    }
    console.log('ilogin created')
    const id = await createToken()
    console.log(id)
    const userPassStr = `{ "username": "${payload.username}", "password": "${payload.password}" }`
    res.cookie("loginToken", id, {signed: true})
    redisClient.set(id, userPassStr)
    redisClient.expire(id, 10)
    return res.status(200).send('you are logged in')
}
// signin with 10 second TTL
app.get('/login/ttl/:user/:pass', async(req: Request, res: Response) => {
    const payload: ILogin = {
        username: req.params.user,
        password: req.params.pass
    }
    console.log('ilogin created')
    const id = await createToken()
    console.log(id)
    const userPassStr = `{ "username": "${payload.username}", "password": "${payload.password}" }`
    res.cookie("loginToken", id, {signed: true})
    redisClient.set(id, userPassStr)
    redisClient.expire(id, 10)
    return res.status(200).send('you are logged in')
})

)

app.get('/checklogin', async(req: Request, res: Response) => {
    if (req.signedCookies.loginToken) {
        const creds = await redisClient.get(req.signedCookies.loginToken)
        console.log(creds)
        return res.status(200).send(`${req.signedCookies.loginToken}\n${creds}`)
    }
    return res.status(400).send("validation failed")
})

app.get('/loggedinview', async(req: Request, res: Response) => {
    if (req.signedCookies.loginToken) {
        //await redisClient.del(req.signedCookies.loginToken)
        const creds = await redisClient.get(req.signedCookies.loginToken)
        if (creds != null){
            const credsObj = JSON.parse(creds)
            return res.status(200).send(`Hi, thanks for logging in ${credsObj.username}`)
        }
        else {
            // this works if client has loginToken that does not find redis match 
            // (see commented out code above for testing)
            return res.redirect(301, "/login/fakeuser/fakepass")
        }
    }
    else {
        // goes here if signature does not match
        return res.status(400).send('no matching cookies')
    }
})

app.listen(PORT, HOST, () => {
    console.log(`App listening on ${HOST}:${PORT}`)
})


/*

this is intentionally written to break the recursive function and never return a successful output

*/
const mockCreateToken = async(testNum: string): Promise<string> => {
    if (!(await redisClient.get(testNum))){
        console.log('in mock')
        testNum = await mockCreateToken(testNum)
    }
    return testNum
}

app.get('/break/:testVar', async(req: Request, res: Response) => {
    const id = await mockCreateToken(req.params.testVar)
    return res.status(200).send('check your console')
})
