import express, { Application, Request, Response } from 'express';
import session from 'express-session';
import Redis from 'ioredis';
import RedisStore from 'connect-redis';

const dbPass = process.env.PASSWORD as string;
const dbHost = process.env.HOST as string;
const dbPort = process.env.PORT as string;

let redisClient = new Redis(
    `redis://default:${dbPass}@${dbHost}:${dbPort}`
);
//redisClient.connect()

const redisStore = RedisStore(session);

const app: Application = express()
    .use(express.json())
    .use(session({
        secret: 'ThisIsHowYouUseRedisStorage',
        name: '_rediPratice',
        resave: false,
        saveUninitialized: true,
        cookie: {
            secure: false
        },
        store: new redisStore({
            client: redisClient,
            ttl: 86400
        })
    }));
const HOST: string = '0.0.0.0'
const PORT: number = 3000;

app.get('/', async(req: Request, res: Response) => {
    return res.status(200).send('a string (with a cookie?)')
})

app.post('/', async(req: Request, res: Response) => {

})

app.listen(PORT, HOST, () =>{
    console.log(`App listening at ${HOST}:${PORT}`)
})
/*
    https://www.geeksforgeeks.org/session-cookies-in-node-js/
    https://blog.devtylerjones.com/sessions_and_cookies_with_node_and_express
    https://expressjs.com/en/resources/middleware/cookie-session.html
    https://medium.com/weekly-webtips/how-to-create-a-simple-login-functionality-in-express-5274c44c20df
    https://catherinefadamiro.medium.com/using-express-session-with-typescript-node-js-application-b31c68c59e16

    ignore those use this
    https://medium.com/mtholla/managing-node-js-express-sessions-with-redis-94cd099d6f2f
--/

import express, { Application, Request, Response } from 'express';
import session from 'express-session';
import redis from 'redis';
import RedisStore from 'connect-redis';

const dbPass = process.env.PASSWORD as string;
const dbHost = process.env.HOST as string;
const dbPort = process.env.PORT as string;

//let redisClient: RedisStore.Client = redis.createClient({
let redisClient = redis.createClient({
    legacyMode: true,
    url: `redis://default:${dbPass}@${dbHost}:${dbPort}`
});
redisClient.connect().catch(console.error);

const redisStore = RedisStore(session);

const app: Application = express()
    .use(express.json())
    .use(session({
        secret: 'ThisIsHowYouUseRedisStorage',
        name: '_rediPratice',
        resave: false,
        saveUninitialized: true,
        cookie: {
            secure: false
        },
        store: new redisStore({
            client: redisClient,
            ttl: 86400
        })
    }));
const HOST: string = '0.0.0.0'
const PORT: number = 3000;

app.get('/', async(req: Request, res: Response) => {
    return res.status(200).send('a string (with a cookie?)')
})

app.post('/', async(req: Request, res: Response) => {

})


app.listen(PORT, HOST, () =>{
    console.log(`App listening at ${HOST}:${PORT}`)
})
*/