const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mysql = require('mysql');
const fs = require('fs');


//parse application/json
app.use(bodyParser.json());

//read config json file
let rawConfig = fs.readFileSync('./config.json');
let confData = JSON.parse(rawConfig);

//create database connection
const conn = mysql.createConnection({
	host: confData["host"], 
	port: confData["port"],
	user: confData["user"],
	password: confData["pass"],
	database: confData["name"]
});

//connect to database
conn.connect((err) =>{
	if(err) throw err;
	console.log(`MySQL database ${confData["name"]} @ ${confData["host"]}:${confData["port"]}`);
});


//show all
app.get('/api/agencies',(req, res) => {
	let sql = "SELECT * FROM Agency";
	let query = conn.query(sql, (err, results) => {
        	if (err) throw err;
	res.send(JSON.stringify(
		{
		"status":200,
		"error": null,
		"response": results
		}));

	});
});

//lookup by id
app.get('/api/id=:id',(req, res) => {
	let sql = `SELECT * FROM Agency WHERE Agency.id LIKE ${req.params.id}`;
	let query = conn.query(sql, (err, results) => {
        	if (err) throw err;
	res.send(JSON.stringify(
		{
		"status":200,
		"error": null,
		"response": results
		}));

	});
});

//lookup by name
app.get('/api/name=:name',(req, res) => {
	let sql = `SELECT * FROM Agency WHERE Agency.name LIKE "*${req.params.name}*"`;
	let query = conn.query(sql, (err, results) => {
        	if (err) throw err;
	res.send(JSON.stringify(
		{
		"status":200,
		"error": null,
		"response": results
		}));

	});
});


//Server listening
app.listen(3121,()=>{
	console.log('Listening on port 3121...');
});

//http://mfikri.com/en/blog/nodejs-restful-api-mysql
