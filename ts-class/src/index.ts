class BankAccount {
    username!: string;
    password!: string;
    balance: number;
    
    constructor(passedUser: string, passedPass: string, passedBalance?: number) {
        this.username = passedUser;
        this.password = passedPass;
        if (passedBalance) {
            this.balance = passedBalance;
        }
        else {
            this.balance = 0;
        }
    }
    changeBalance(amount: number, deposit: boolean): void {
        if (deposit) {
            this.balance += amount;
        }
        else {
            this.balance -= amount;
        }
    }
}

class SavingsAccount extends BankAccount {
    interest: .01 | .001;

    constructor(passedUser: string, passedPass: string, highYield: boolean, passedBalance?: number)  {
        super(passedUser, passedPass, passedBalance);
        if (highYield) {
            this.interest = .01;
        }
        else {
            this.interest = .001;
        }
    }

}

class CreditAccount extends BankAccount {
    interest: .15 | .25;
    reward: "miles" | "cash back";
    points: number;

    constructor(passedUser: string, passedPass: string, goodCredit: boolean, miles: boolean, pointBonus?: number) {
        super(passedUser, passedPass);
        if (goodCredit) {
            this.interest = .15;
        }
        else {
            this.interest = .25;
        }
        if (miles) {
            this.reward = "miles"
        }
        else {
            this.reward = "cash back"
        }
        if (pointBonus) {
            this.points = pointBonus
        }
        else {
            this.points = 0
        }
    }
}

let bankUser0 = new BankAccount("Jared", "securePass")
let bankUser1 = new BankAccount("Jared", "securePass", 300)

console.log(bankUser0)
console.log(bankUser1)

bankUser0.changeBalance(200, true)
bankUser1.changeBalance(200, false)

console.log(bankUser0)
console.log(bankUser1)

const creditUser = new CreditAccount("jaredCredit", "securePass", true, false, 1500)
const savingsUser = new SavingsAccount("jaredSavings", "securePass", true, 5000)

console.log(creditUser)
console.log(savingsUser)

creditUser.changeBalance(200, false)
savingsUser.changeBalance(1000, true)

console.log(creditUser)
console.log(savingsUser)
