import { randpix, RandpixColorScheme, Symmetry } from 'randpix'
import * as crypto from 'crypto'
import fs from 'fs'


const generate = randpix({
    //colorScheme: RandpixColorScheme.DARKULA,
    size: 8,
    scale: 32,
    symmetry: Symmetry.VERTICAL,
    seed: crypto.randomBytes(64)
        .toString('base64'),
    colorBias: 15,
    grayscaleBias: false
})

const art = generate()

const pngBuffer = art.toBuffer('image/png')

fs.writeFile(`${process.cwd()}/new.png`, pngBuffer, (err) => {
    if (err) console.log(err)
})
