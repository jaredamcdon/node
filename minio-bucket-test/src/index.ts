import * as minio from 'minio';
import stream from 'stream';
import fs from 'fs';
import { ReadStream } from 'fs'

const command: string = process.argv[2]

console.log(`command: ${command}`)

const minioClient = new minio.Client({
    endPoint: "10.0.0.25",
    port: 900,
    useSSL: false,
    accessKey: "jared",
    secretKey: "dev_pass"
})
/*
const minioClient = new minio.Client({
    endPoint: "10.0.0.25",
    port: 9080,
    useSSL: false,
    accessKey: "IJ0cDI9cnoRc9K0R",
    secretKey: "RdfOF6YQdacv39HA3EPaFdoSJsEFJuSo"
})
*/
interface fileObject {
    name: string;
    file: ReadStream;
}

const water: fileObject = {
    name: "water.png",
    file: fs.createReadStream(process.cwd()+'/src/img/water.png')
}
const fire: fileObject = {
    name: "fire.png",
    file: fs.createReadStream(process.cwd()+'/src/img/fire.png')
}
const earth: fileObject = {
    name: "earth.png",
    file: fs.createReadStream(process.cwd()+'/src/img/earth.png')
}
const air: fileObject = {
    name: "air.png",
    file: fs.createReadStream(process.cwd()+'/src/img/air.png')
}

const metaData = {
    'Content-Type': 'image/png'
}

switch (command) {
    case "bucketPolicy":
        //minioClient.makeBucket('test', '')
        const bucketName = 'test'
        const testPolicy = {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Action": [
                        "s3:GetBucketLocation",
                        "s3:ListBucket"
                    ],
                    "Effect": "Allow",
                    "Principal": {
                        "AWS": [
                            "*"
                        ]
                    },
                    "Resource": [
                        `arn:aws:s3:::${bucketName}`
                    ],
                    "Sid": ""
                },
                {
                    "Action": [
                        "s3:GetObject"
                    ],
                    "Effect": "Allow",
                    "Principal": {
                        "AWS": [
                            "*"
                        ]
                    },
                    "Resource": [
                        `arn:aws:s3:::${bucketName}/*`
                    ],
                    "Sid": ""
                }
            ]
        }

        minioClient.setBucketPolicy('test', JSON.stringify(testPolicy), (err) => {
            if (err) throw err

            console.log('Set bucket policy')
        })

        const testPolicies = minioClient.getBucketPolicy('test', (err, policy) => {
            if (err) throw err

            console.log(typeof policy)
            console.log(policy)
        })

        break;
    case "bucket":
        minioClient.makeBucket('user-images', "")

        break;
    case "policy":
        const policy = {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Action": [
                        "s3:GetBucketLocation",
                        "s3:ListBucket"
                    ],
                    "Effect": "Allow",
                    "Principal": {
                        "AWS": [
                            "*"
                        ]
                    },
                    "Resource": [
                        "arn:aws:s3:::user-images"
                    ],
                    "Sid": ""
                },
                {
                    "Action": [
                        "s3:GetObject"
                    ],
                    "Effect": "Allow",
                    "Principal": {
                        "AWS": [
                            "*"
                        ]
                    },
                    "Resource": [
                        "arn:aws:s3:::user-images/*"
                    ],
                    "Sid": ""
                }
            ]
        }

        minioClient.setBucketPolicy('user-images', JSON.stringify(policy), (err) => {
            if (err) throw err

            console.log('Set bucket policy')
        })

        const policies = minioClient.getBucketPolicy('user-images', (err, policy) => {
            if (err) throw err

            console.log(policy)
        })

        break;
    case "upload":
        const fireFile = async () => {
            return await minioClient.putObject('user-images', fire.name, fire.file, metaData);

        }
        console.log(fireFile)

        minioClient.putObject('user-images', water.name, water.file, metaData);
        
        minioClient.putObject('user-images', earth.name, earth.file, metaData);

        minioClient.putObject('user-images', air.name, air.file, metaData);
        
        //const fakeFile: boolean = false;
        //minioClient.putObject('user-images', water.name, fakeFile, metaData);

        console.log(`with policy command set, url is: 10.0.0.25:900/user-image/${air.name}`)
        console.log("${host}:${port}/${bucket}/${file}")

        break;
    case "download":
        // i dont fully understand this one
        const fireGet = minioClient.getObject('user-images', fire.name, (err, datastream) => {
            if (err) {
                return console.log(err)
            }
            let size = 0
            datastream.on('data', (chunk) => {
                size += chunk.length
            })
            datastream.on('end', () => {
                console.log(size)
            })
            datastream.on('error', () => {
                console.log(err)
            })
        });
        

        const waterGet = minioClient.getObject('user-images', water.name);
        
        const earthGet = minioClient.getObject('user-images', earth.name);

        break;
    case "remove":
        minioClient.removeObject('user-images', fire.name);
        console.log(`Removed object: ${fire.name}`);
        
        minioClient.removeObject('user-images', water.name);
        console.log(`Removed object: ${water.name}`);
        
        minioClient.removeObject('user-images', earth.name);
        console.log(`Removed object: ${earth.name}`);

        break;
    case "url":
        // time is in seconds
        minioClient.presignedUrl('GET', 'user-images', fire.name, 60*10, (err, presignedUrl) => {
            if (err) return console.log(err)
            console.log(presignedUrl)
        })
        minioClient.presignedUrl('GET', 'user-images', water.name, 60*10, (err, presignedUrl) => {
            if (err) return console.log(err)
            console.log(presignedUrl)
        })
        minioClient.presignedUrl('GET', 'user-images', earth.name, 60*10, (err, presignedUrl) => {
            if (err) return console.log(err)
            console.log(presignedUrl)
        })
        break;
    default:
        console.log(`no match for ${command}`)
}
