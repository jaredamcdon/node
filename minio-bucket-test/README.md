## scripts

### make bucket
```typescript
npm run bucket
```

### set bucket policy
```typescript
npm run policy
```

### upload files
```typescript
npm run upload
```

### download files
```typescript
npm run download
```

### remove files
```typescript
npm run remove
```

### get object url
```typescript
npm run url
```
