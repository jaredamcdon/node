# docker compose
> how to set up this docker container locally on any docker server
## starting the container
> the bash commands
1. Docker Command:
```sh
docker-compose up -d
```
2. Start container in bash:
```sh
docker exec -it jet350_mongoDB bash
```
## Setting up the database
> get the database to a starting point with a root user
1. Starting mongo without access control
```sh
mongod --port 27017 --dbpath /var/lib/mongodb
```
2. Connect to the instance
```sh
mongo --port 27017
```
3. Create admin user(s)
```sh
use admin
db.createUser(
  {
    user: "myUserAdmin",
    pwd: passwordPrompt(), // or cleartext password
    roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]
  }
)
```
4. Restart the instance
>I'm still fuzzy on this one
```sh
mongod --auth --port 27017 --dbpath /var/lib/mongodb
```
## Connecting to the Database
>mongo connection syntax is one line and offbeat
```sh
mongodb://jaredRemote:*****@192.168.3.25:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false
```
Your entry point will vary - heres the key point
* mongodb://
* username:password
* @serverIP:portnumber
```sh
mongodb://username:password@serverIP:portnumber
```

## Source
[docs.mongodb.com](https://docs.mongodb.com/manual/tutorial/enable-authentication/)