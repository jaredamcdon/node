# jet350
>Testing site for future Wix replacement

![jet](./hero-min.jpg)

## Current Link
>Private link - only accesible to those with repository permissions\
[jet350 static pages](https://jet350.gitlab.io/jet350/)

## Framework
Currently serverless static hosting\
Options:
* Node.js
    * Express.js framework
        * most efficient
* Python
    * Flask
        * Gives option for frozen-flask
            * can reduce overhead
* PHP
    * Probably not
* Ruby
    * Ruby on Rails
        * Unknown development time
        * Extremely efficient

## Current Resources
* SVGs
    * [Hero Icons](https://heroicons.com/)
* Images
    * [Website OneDrive](https://onedrive.live.com/?authkey=%21AigtpOJpqKxU2cQ&id=E3C1D9D0B1733D51%21156295&cid=E3C1D9D0B1733D51)
    * [BG Pics](https://onedrive.live.com/?cid=e3c1d9d0b1733d51&id=E3C1D9D0B1733D51%21136867&authkey=%21AgoC4Jbzojeh2t0&v=photos)
* Tools
    * [jpeg compressing](https://compressjpeg.com/)
* Wix
    * [Site](https://users.wix.com/signin?view=sign-up&sendEmail=true&postSignUp=https:%2F%2Fwww.wix.com%2Fnew%2Fintro&loginCompName=Signup_H&referralInfo=Signup_H&postLogin=https:%2F%2Fwww.wix.com%2Fmy-account%2Fsites&forceRender=true)
        * <details>
            <summary>credentials</summary>
            <p>User - mmcdonald2@outlook.com</p>
            <p>Pass - vcbxJ8dSVifbdUy</p>
          </details>

## Using git
Pull updates:
```sh
git pull
```
Push updates:
```sh
git add .
git commit -a
git push
```