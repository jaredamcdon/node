//constants
const express = require ('express');
const app = express();
const port = 4545;
const path = require('path');

//middleware--------------------
//pug
app.set('view engine', 'pug');
app.set('views', './views');

//set static directory
app.use(express.static(path.join(__dirname,'/public')));

//routing

app.get('/', function(req, res){
    res.render('pages/home');
});
app.get('/login', function(req,res){
    res.render('pages/login')
})
app.get('/forgot', function(req,res){
    res.render('pages/forgotPassword')
})

app.listen(port, ()=> console.log(`app listening at http://127.0.0.1:${port}`));