module.exports = (function(){
    'use strict';
    const router = require('express').Router();

    //index
    router.get('/', function(req,res){
        res.render('pugHeader')
    });

    return router;
})();