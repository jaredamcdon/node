var cmsEle = document.body.querySelector('#cartMenuSelect');
var navDivEle = document.body.querySelector('#navDiv');

var openedWithNav = false;
function toggleCMS(){
    if(cmsEle.style.visibility == 'hidden' || cmsEle.style.visibility == '' ){
        cmsEle.style.visibility = 'visible';
        cmsEle.style.opacity = '1';
        openedWithNav = false;
    }
    else{
        cmsEle.style.visibility = 'hidden';
        cmsEle.style.opacity = '0';
        openedWithNav = false;
    }

}
function closeCMS(){
    cmsEle.style.visibility = 'hidden';
    cmsEle.style.opacity = '0';
    openedWithNav = false;
}


function toggleNav(){
    if(navDivEle.style.visibility == 'hidden' || navDivEle.style.visibility == ''){
        navDivEle.style.visibility = 'visible';
        cmsEle.style.visibility = 'visible';
        navDivEle.style.opacity = '1';
        cmsEle.style.opacity = '1';
        openedWithNav = true;
    }
    else{
        navDivEle.style.visibility = 'hidden';
        navDivEle.style.opacity = '0';
        if(openedWithNav == true){
            cmsEle.style.visibility = 'hidden';
            cmsEle.style.opacity = '0';
            //openedWithNav = false;
        }
    }
}
/*
function open(passed){
    console.log(passed)
    if(passed == 'CMS' || passed == 'navDiv'){
        cmsEle.style.visibility = 'visible';
    }
	if(passed == 'navDiv'){
        navDivEle.style.visibility = 'visible';
    }
}
*/
