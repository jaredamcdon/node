# Node.js
>Server side rendering with the help of ExpressJS and Middleware

## Why Node.js
>Node.js is fast and efficient from both a development and serving solution
* Node.js: 
    * has great functionality with nosql such as MongoDB
    * has multiple templating solutions
    * unifies front-end and back-end languages to use JavaScript 
* How do I learn more?
    * [Node.js Documentation](https://nodejs.org/en/docs/)

## NPM
>Node Package Manager
* What is NPM?
    * NPM is how you install middleware to add additional functionality to your code
    * NPM has similar functionality to apt or pip but for Node.js
## Express
What is Express?
* Express is middleware for Node.js to easily route your website efficiently
* Express handles routing, ports and more
* How do I learn more?
    * [Express Documentation](https://expressjs.com/)
    * [TutorialsPoint](https://www.tutorialspoint.com/nodejs/nodejs_express_framework.htm)
## Pug
What is Pug?
* Pug is a templating engine with functionality similar to PHP
* Pug can dynamically create webpages based off of mixins and database calls
* How do I learn more?
    * [Pug Documentation](https://pugjs.org/api/getting-started.html)
    * [Scriptverse](https://scriptverse.academy/tutorials/nodejs-express-pug.html)
 ## Running a Node.js Application
 >Using BASH and a terminal you can get a node application running in seconds
* Note - this is for Ubuntu/apt package manager
1. `sudo apt update`
2. `sudo apt install nodejs`
3. `sudo apt install npm`
4. `nodejs {app.js}`


 ## Future - PM2
 >PM2 looks like a good option for hosting Node.js projects
 * [PM2 Article](https://www.serverlab.ca/tutorials/linux/administration-linux/how-to-run-nodejs-applications-in-production-using-pm2/)