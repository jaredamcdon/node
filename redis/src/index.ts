import  { createClient } from 'redis'
import express, { Application, Request, Response } from 'express'


// redis
const dbHost = process.env.HOST as string;
const dbPort = process.env.PORT as string;
const dbPass = process.env.PASSWORD as string;

const client = createClient({
    url: `redis://default:${dbPass}@${dbHost}:${dbPort}`
});

client.on('error', (err: any) => console.log('Redis client error', err))

const connect = async() => {
    await client.connect().then(() =>{
        console.log('client connected')
    });
}
connect()

const setGet = async() => {
    await client.set('lemon', 'fruit')
    console.log(await client.get('lemon'))
}
setGet()
// express
const HOST = '127.0.0.1';
const PORT = 5003;

const app: Application = express()
    .use(express.json());

app.post('/set', async (req: Request, res: Response) => {
    const key = req.body.key
    const value = req.body.value
    console.log(`key:\t${key}\nval:\t${value}`)
    const dbResponse = await client.set(key, value)
    return ( dbResponse == 'OK' ? res.status(200).send(dbResponse) : res.status(400).send() )
})

app.get('/get/:key', async (req: Request, res: Response) => {
    const key = req.params.key
    const value = await client.get(key)
    return ( value ? res.status(200).send(value) : res.status(400).send() )
})

app.get('/test', async (req: Request, res: Response) => {
    return res.status(200).send('works')
})

app.listen(PORT, HOST, () => {
    console.log(`App listening at ${HOST}:${PORT}`)
})