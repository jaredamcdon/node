import axios from 'axios'
import { JSDOM } from 'jsdom'
import puppeteer from 'puppeteer'


const puppetPage = async (url: string): Promise<string | undefined> => {
    const browser = await puppeteer.launch({
        headless: true
    })

    const page = await browser.newPage()
    await page.goto(url, {waitUntil: 'networkidle0'})

    const bodyHandle = await page.$('body')
    const html = await page.evaluate(body => body?.innerHTML, bodyHandle)

    const dom = new JSDOM(html)
    let scrapedDom = dom.window.document

    const getImg = (): HTMLImageElement | null => {
        return scrapedDom.querySelector('img')
    }
    let firstImg: HTMLImageElement | null = null
    for (let attempts = 0; attempts <= 5; attempts++){
        firstImg = getImg()
        if (
            firstImg != null
            && !firstImg.getAttribute('src')?.includes("cookielaw")
            && !firstImg.getAttribute('src')?.includes("t.co")
        ){
            await browser.close();
            const firstImgSrc = firstImg.getAttribute('src')
            if (firstImgSrc != null){
                return firstImgSrc
            }
        }
        setTimeout(() => {}, 100)
    }
    await browser.close();
    return undefined
}

// #############################

const fetchPage = async (url: string): Promise<string | undefined> => {
    const HTMLData = axios
        .get(url)
        .then(res => res.data)
        .catch((error: any) => {
            console.error(`err: ${error}`)
        });
    return HTMLData
}

const getFirstLogo = async (htmlData: string): Promise<string | undefined> => {
    const dom = new JSDOM(htmlData)
    let scrapedDom = dom.window.document
    let imgTags: HTMLImageElement[] = Array.from(scrapedDom.querySelectorAll('img.sd-image'),);
    for (let i=0; i < imgTags.length; i++){
        let imgUrl = imgTags[i].getAttribute('data-src')
        if (imgUrl != undefined){
            return imgUrl
        }
    }
    // the better implementation of this is to have this as a file that couldnt
    // be a container name like "u882-asd-c998-c0lq.png" and do a copy using 
    // minio / something like -> `cp "u882-asd-c...png" "jmusicbot"`
    return "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fgame-icons.net%2Ficons%2F000000%2Ftransparent%2F1x1%2Fdelapouite%2Fcargo-crate.png&f=1&nofb=1&ipt=b3830751b70e9ca146b26587682ee6cc360b0b0a868d3a505ec872502d6b0ec0&ipo=images"
    return undefined
}

// #############################

const scrapeWikimedia = async (query: string): Promise<string | undefined> => {
    let page = await fetchPage(`https://commons.wikimedia.org/w/index.php?search=${query}&title=Special:MediaSearch&go=Go&type=image&filemime=svg`)
    if (page != undefined){
        return getFirstLogo(page)
    }
    return undefined
}

const scrapeDockerHub = async (query: string): Promise<string | undefined> => {
    let src = await puppetPage(`https://hub.docker.com/search?q=${query}`)
    if (src != null && src != undefined){
        return src
    }
    return undefined
}

// main

const main = async (query: string) => {
    console.log('dockerhub')
    let scrapedLink = (await scrapeDockerHub(query))
    if (scrapedLink == undefined){
        console.log('wikimedia')
        scrapedLink = await scrapeWikimedia(query)
    }
    console.log(scrapedLink)
}

main('mongodb')
main('mysql')
main('jmusicbot')
main('handbrake')