#define twenties variable
twenties = 0
#define tens variable
tens = 0
#define fives variable
fives = 0
#define ones
ones = 0
#define quarters
quarters = 0
#define dimes
dimes = 0
#define nickels
nickels = 0
#define pennies
pennies = 0

#define input itemprice with output question
itemprice = input("How much does the item cost?\n$")
#define cashtendered with output question
cashtendered = input("Cash tendered:\n$")
#convert itemprice from string to float
itemprice = float(itemprice)
#convert cashtendered from string to float
cashtendered = float(cashtendered)
#create variable changedollars by subtracting itemprice from cashtendered
changedollars = cashtendered - itemprice
#print change in dollars and cents
print("Change: ${}".format(changedollars))
#create variable that puts change into cents
changecents = int(changedollars * 100)
#print change in cents
print("Change in cents: {}".format(changecents))

#convert change into bills and coins

#convert change into twenties
#if change >= 2000 continue
while (changecents >= 2000):
  #add one to twenties variable
  twenties+=1
  #subtract 2000 from changecents
  changecents = changecents - 2000

#convert change into tens
#if change >= 1000 continue
while (changecents >= 1000):
  #add one tens variable
  tens+=1
  #subtract 1000 from changecents
  changecents = changecents - 1000

#convert change into fives
#if change >= 500 continue
while (changecents >= 500):
  #add one to fives variable
  fives+=1
  #subtract 500 from changecents
  changecents = changecents - 500

#convert change into ones
#if change >= 100 continue
while (changecents >= 100):
  #add one to ones variable
  ones+=1
  #subtract 100 from changecents
  changecents = changecents - 100

#convert change into quarters
#if change >= 25 continue
while (changecents >= 25):
  #add one to quarters variable
  quarters+=1
  #subtract 25 from changecents
  changecents = changecents - 25

#convert change into dimes
#if change >= 10 continue
while (changecents >= 10):
  #add one to dimes variable
  dimes+=1
  #subtract 10 from changecents
  changecents = changecents - 10

#convert change into nickels
#if change >= 5 continue
while (changecents >= 5):
  #add one to nickels variable
  nickels+=1
  #subtract 5 from changecents
  changecents = changecents - 5

#convert change into pennies
#if change >= 1 continue
while (changecents >= 1):
  #add one to pennies variable
  pennies+=1
  #subtract 1 from changecents
  changecents = changecents - 1

#output twenties variable
print("Twenties: {}".format(twenties))
#output tens variable
print("Tens: {}".format(tens))
#output fives variable
print("Fives: {}".format(fives))
#output ones variable
print("Ones: {}".format(ones))
#output quarters variable
print("Quarters: {}".format(quarters))
#output dimes variable
print("Dimes: {}".format(dimes))
#output nickels variable
print("Nickels: {}".format(nickels))
#output pennies variable
print("Pennies: {}".format(pennies))
