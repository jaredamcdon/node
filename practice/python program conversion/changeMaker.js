const fs = require(`fs`);
const commandLineArgs = require(`command-line-args`);

//to run through loops
var questionsToAsk = [
    {
        //define itemPrice as variable
        name: 'itemPrice',
        type: String
    },
    {
        //define cashTendered as variable
        name: 'cashTendered',
        type: String
    },
    {
        name: 'twenties',
        type: Number
    },
    {
        name: 'tens',
        type: Number
    },
    {
        name: 'fives',
        type: Number
    },
    {
        name: 'ones',
        type: Number
    },
    {
        name: 'quarters',
        type: Number
    },
    {
        name: 'dimes',
        type: Number
    },
    {
        name: 'nickels',
        type: Number
    },
    {
        name: 'pennies',
        type: Number
    },
    {
        name: `total`,
        type: Boolean
    }
];

//define denomination variables
var twenties = 0;
var tens = 0;
var fives = 0;
var ones = 0;
var quarters = 0;
var dimes = 0;
var nickels = 0;
var pennies = 0;

const answers = commandLineArgs(questionsToAsk);

//  node changeMaker.js
//  node changeMaker.js --price=[price]
//  node changeMaker.js --tendered=[amountTendered]
//  node changeMaker.js --total


let getJson = fs.readFileSync(`changeMaker.json`);
let data = JSON.parse(getJson);

const saveData = (newData) =>{
    const toString = JSON.stringify(newData);
    fs.writeFileSync('changeMaker.json', toString);
}

if(answers.itemPrice){
    data.itemPrice = answers.itemPrice;
    console.log("Cash tendered:\nEnter Price as --cashTendered=");
    saveData(data);
}
else if(answers.cashTendered){
    data.cashTendered = answers.cashTendered;
    var totalCost = (data.cashTendered - data.itemPrice) * 100;

    while (totalCost >= 2000){
        twenties++;
        totalCost = totalCost - 2000;
    }
    while (totalCost >= 1000){
        tens++;
        totalCost = totalCost - 1000;
    }
    while (totalCost >= 500){
        fives++;
        totalCost = totalCost - 500;
    }
    while (totalCost >= 100){
        ones++;
        totalCost = totalCost - 100;
    }
    while (totalCost >= 25){
        quarters++;
        totalCost = totalCost - 25;
    }
    while (totalCost >= 10){
        dimes++;
        totalCost = totalCost - 10;
    }
    while (totalCost >= 5){
        nickels++;
        totalCost = totalCost - 5;
    }
    while (totalCost >= 1){
        pennies++;
        totalCost = totalCost - 1;
    }
    data.twenties = twenties;
    data.tens = tens;
    data.fives = fives;
    data.ones = ones;
    data.quarters = quarters;
    data.dimes = dimes;
    data.nickels = nickels;
    data.pennies = pennies;
    saveData(data);
    console.log(`Thank you!\nTo see your total please enter --total`)
}
else if(answers.total){
    console.log(data);
    console.log(`The total was $${data.itemPrice}, you paid $${data.cashTendered}. Your change totals $${data.cashTendered - data.itemPrice}`);
    data.itemPrice = '';
    data.cashTendered = '';
    data.twenties = 0;
    data.tens = 0;
    data.fives = 0;
    data.ones = 0;
    data.quarters = 0;
    data.dimes = 0;
    data.nickels = 0;
    data.pennies = 0;
    saveData(data);
}
else{
    console.log("How much does the item cost?\nEnter Price as --itemPrice=");
}
