//you can use const or var
//os comes from the node.js API - documentation can be found on nodejs.org webpage
const os = require('os');
const fs = require('fs');

let user = os.userInfo();

console.log('Hello world\n');

console.log(user);
console.log('user.username: ' + user.username);

let platform = os.platform();

console.log('console.log os.platform: ' + platform);

//this is a js function
let date = new Date();

//the ${} is ES6 ALSO - the ` button above tilde NOT the ' button above "
let message = `User ${user.username} started app at ${date}\n`;

/*
This creates a file
    First argument ('hello.txt') creates file using fs mething
    Second argument inserts message variable into the first argument (hello.txt)
*/
fs.appendFile('hello.txt',message,(err)=>{
    //the third argument (err) is designed for error handling - ES6
    if(err){
        console.log('error');
    }
})
