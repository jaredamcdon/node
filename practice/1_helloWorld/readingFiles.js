const os = require('os');
const fs = require('fs');

//connect user.js to this program when called
const userData = require('./user.js');


//this will run user.js in console
console.log(userData);

//module includes information about the readingFiles.js file AS WELL as user.js because it was called above
//console.log(module);


console.log(userData.user);

console.log(userData.user.name);

let message = `User: ${userData.user.name}`;


//see users.js for userData.addLog
if(userData.addLog){
    fs.appendFile("appended.txt", message, (err)=>{
        if(err){
            console.log('error');
        }
    })
}
