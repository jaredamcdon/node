
const fs = require(`fs`);

const commandLineArgs = require(`command-line-args`);

const optionsDefinitions = [
    {
        name:'name',
        type: String
    },
    {
        name:'order',
        type: String
    },
    {
        name: 'payment',
        type: String
    },
    {
        name: 'exit',
        type: Boolean
    }
];

const options = commandLineArgs(optionsDefinitions);

//console.log(options);

// 1 - node app.js
// 2 - node app.js --name=James
// 3 - node app.js --order=pizza
// 4 - node app.jus --payment=100
// 5 - node app.js --exit

let getJson = fs.readFileSync(`db.json`);
let data = JSON.parse(getJson);

const saveIt = (newData) =>{
    const toString = JSON.stringify(newData);
    fs.writeFileSync(`db.json`, toString);
}
//console.log(data);

if(options.name){

    data.name = options.name;

    console.log(`Hello, ${options.name}, we are serving CAKE, PIZZA, SALAD`);

    saveIt(data);

}
else if(options.order) {

    data.order = options.order;

    console.log(`Okay, ${data.name}, that will be $25 for your ${data.order}, you will pay with...`);

    saveIt(data);
}
else if(options.payment){

    data.payment = options.payment;

    console.log(`Your change is $${options.payment - 25}, thanks for eating at chuckies type --exit to get the order`);

    saveIt(data);
}
else if(options.exit){
    console.log(data);
    console.log(`Thanks`);

    data.name = '';
    data.order = '';
    data.payment = '';
    saveIt(data);
}
else {
    console.log(`Hello, please enter your name`);
}

//console.log(process.argv);

/* if you run above command in terminal as [node app.js name=jared]
output:
[
  'C:\\Program Files\\nodejs\\node.exe',
  'C:\\Users\\jared\\Documents\\codingProjects\\node.js\\practice\\2_foodBot\\app.js',
  'name=jared'
]
*/
