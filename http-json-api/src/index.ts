import express, { Application, Request, Response } from 'express';
// original import - fetch from 'node-fetch' => module error
import fetch from 'node-fetch-commonjs';

// app init and configs
const app: Application = express()
    // bug - forgot to import => no request body
    .use(express.json());
const HOST: string = "127.0.0.1";
const PORT: number = 3000;

/* personal implementation
    if unable to change ingest, create input and storage
    interface with id as string for input and id as number
    (int) for storage interface

    could also be solved by converting to int when going 
    into db interface and converting on the way out (if necessary)
*/
interface IAccountHoldings {
    id: string;
    holdings: [{
        security: string;
        shares: number;
    }],
    owner: Array<string>;
}

const holdingsController = async(holdingsInput: Array<IAccountHoldings>): Promise<string> => {
    let ownersObj: {
        [key: string]: Array<string>
    } = {}

    let multiOwner: Array<string> = [];

    for (let i in holdingsInput){
        // if multiple owners of security store for later usage
        if(holdingsInput[i].owner[1]) {
            for (let val = 0; val < holdingsInput[i].owner.length; val++){
                multiOwner.push(holdingsInput[i].owner[val])
            }
        }
        // add to ownersObj new key of owner from current holding Object
        // set key to id of account
        ownersObj[`${holdingsInput[i].owner}`] = [holdingsInput[i].id];
    }
    
    // if multiple owners present convert individuals to a multi-owner configuration
    if (multiOwner != []){
        // would be nested if handling multiple Array<Array<string>> => for (let i in multiowner) { for (let x in multiowner[i]) { logic on multiowner[i][x] } }
        for (let i in multiOwner){
            // check to see if one of owners has other account IDs
            if (multiOwner[i] in ownersObj){
                /*push value from single account to combined account
                    ownersObj["A,B"].push(ownersObj["A"][0]) => 1 */
                ownersObj[`${multiOwner}`].push(ownersObj[`${multiOwner[i]}`][0])
                // remove single owner account - now stored as multi-owner account
                delete ownersObj[`${multiOwner[i]}`]
            }
        }
    }

    let returning: string = ""
    for (let i in ownersObj){
        // nice to have => more handling for user(s) and account(s)
        if (i.includes(",")){
            returning+=(`Household with users ${i.replace(',', ' and ')} and accounts ${ownersObj[i].sort()}<br>`)
        }
        else {
            returning+=(`Household with user ${i} and account ${ownersObj[i].sort()}`)
        }
    }
    return(returning)
}

app.post('/holdings', async (req: Request, res: Response) => {
    // this assumes some safety on inputs typing and style
    const payload:Array<IAccountHoldings> = req.body
    // could also cast to a class depending on methodology
    const result = await holdingsController(payload)
    return res.status(200).send(result)
})

app.get('/holdings/test', async (req: Request, res: Response) => {
    const testInput: Array<IAccountHoldings> = [
        { id: "1", holdings: [{ security: "AAPL", shares: 12}], owner: ["A"]},
        { id: "2", holdings: [{ security: "GOOG", shares: 32}], owner: ["A", "B"]},
        { id: "3", holdings: [{ security: "AMZN", shares: 11}], owner: ["B"]},
        { id: "4", holdings: [{ security: "TSLA", shares: 45}], owner: ["C"]}
    ]
    const internalResponse = await fetch("http://127.0.0.1:3000/holdings", {
        method: 'POST',
        body: JSON.stringify(testInput),
        headers: {'Content-type': 'application/json'}
    })
    const data = await internalResponse.text()
    console.log(data.replace('<br>', '\n'))
    return res.status(200).send(data)

})

app.listen(PORT, HOST, () => {
    console.log(`App listening at ${HOST}:${PORT}\n(Routes at ['/holdings', '/holdings/test'])`)
})