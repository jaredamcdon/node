## sample websocket / express server

### The guide:
[WebSocket + Node.js + Express — Step by step tutorial using Typescript](https://medium.com/factory-mind/websocket-node-js-express-step-by-step-using-typescript-725114ad5fe4)

### The Dependencies:
> Or just check package.json

- TypeScript
- Express
- http
- ws
- ts-node

### Compiling .ts on Node
- if ts-node installed (it is):
  - update package.json start script (done)
  - this will only find errors WHEN they occur, not before they occur
  - `npm start`
  - DO NOT use in production
    - ts-node is a a JIT compiler
- if tsc installed globally:
  - `tsc`
- if tsc installed locally only
  - `node ./dist/server/server.js`

### client messages for websocket:

```json
{
  "content":"string",
  "isBroadcast":true/false,
  "sender":"string"
}
```
