import * as express from 'express';
import * as http from 'http';
import * as WebSocket from 'ws';

const HOST = '0.0.0.0';
const PORT = 8999;

const app = express();

//initialize simple http server
const server = http.createServer(app);

//initialize WebSocket serve instance
const wss = new WebSocket.Server({ server });

interface ExtWebSocket extends WebSocket {
	isAlive: boolean;
};

function createMessage(content: string, isBroadcast = false, sender = 'NS'): string {
	return JSON.stringify(new Message(content, isBroadcast, sender));
}

export class Message {
	constructor(
		public content: string,
		public isBroadcast = false,
		public sender: string
	) { }
}

wss.on('connection', (ws: WebSocket) => {

	const extWs = ws as ExtWebSocket;

	extWs.isAlive = true;

	ws.on('pong', () => {
		extWs.isAlive = true;    
	});

	ws.on('message', (msg: string) => {
		// log the received message and send it back to the client
		console.log(`recieved: ${msg}`);
		
		const message = JSON.parse(msg) as Message;
		
		if (message.isBroadcast) {
			//send back message to other clients
			wss.clients.forEach(client => {
				if (client != ws) {
					client.send(
						createMessage(message.content, true, message.sender)
					);
				}			
			});
		}
		else {
			ws.send(`Hello, you sent -> ${message}`);
		}
	});

	// send immediately a feedback to the incoming connection
	ws.send('Hi there, I am a WebSocket server');

	ws.on('error', (err) => {
		console.warn(`Client disconnected - reason: ${err}`);    
	})
});

setInterval(() => {
	wss.clients.forEach((ws: WebSocket) => {
		const extWs = ws as ExtWebSocket;

		if (!extWs.isAlive) return ws.terminate();

		extWs.isAlive = false;
		ws.ping(null, undefined);
	});
}, 10000);

//start the server
server.listen(PORT, HOST, () => {
	console.log(`Server stated on ${HOST}:${PORT}`);
});
